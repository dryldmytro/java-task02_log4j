package com.dmytrodryl.application;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID="xxxxxxxxxxx";
    public static final String AUTH_TOKEN="xxxxxxxxxxx";

    public static void send(String str){
        Twilio.init(ACCOUNT_SID,AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380"),
                new PhoneNumber("+1234567890"),str).create();
    }
}
